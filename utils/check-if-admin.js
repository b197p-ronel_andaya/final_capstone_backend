const auth = require("../auth");

function isAdmin(req) {
  const user = auth.decode(req.headers.authorization);
  return user.isAdmin;
}

module.exports = isAdmin;
