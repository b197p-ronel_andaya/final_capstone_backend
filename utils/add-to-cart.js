const Order = require("../models/order.model");
const ordersUtil = require("./orders-util");
const Product = require("../models/product.model");

class Store{
    constructor(request,userData, cartPool){
        this.userRequest = request,
        this.userData = userData,
        this.cartPool = cartPool
    }
    async user(){
        let totalAmount =  ordersUtil.getTotal(this.userRequest);
        let products = ordersUtil.getProducts(this.userRequest);
        const request =  new Order({
            user_id: this.userData.id,
            products: products,
            total_amount: totalAmount,
        });
        return request;
    }
    async product(){
      const productId =   this.userRequest.body.products[0].productId
      const product =   await Product.find({
            _id: productId,
            is_active: true,
          });
          return product;
    }

    async cart(){
        const cart = await this.cartPool.filter(obj => obj.user_id == this.userData.id);
        return cart;
    }

    async checkUser(){
        if(this.userData.isAdmin){
            console.log('first stage');
        return this.cartPool
        }
        console.log('second stage');
        // const newCartpool = await this.cartPool.filter(obj => obj.user_id == this.userData.id);
        return  await this.cartPool.filter(obj => obj.user_id == this.userData.id);
    }
}


module.exports = Store;
