const Product = require("../models/product.model");
const isAdmin = require("../utils/check-if-admin");
const auth = require("../auth");
const url = require("url");

async function createProduct(req, res, next) {
  if (!isAdmin(req)) {
    return res.status(401).send({ isAdmin: false });
  }

  try {
    let productData = {
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
      quantity: req.body.quantity,
    };
    if (req.body.image === "null") {
    } else {
      productData.img_path = `images/products/${req.file.filename}`;
    }
    const newProduct = new Product(productData);
    result = await newProduct.save();
    res.send(true);
  } catch (error) {
    return next(error);
  }
}

async function getAllProducts(req, res, next) {
  try {
    let products;
    if (!req.body.isAdmin) {
      products = await Product.find({ is_active: true }).sort({updated_on: -1});
    } else {
      products = await Product.find({}).sort({updated_on: -1});
    }

    if (products.length === 0) {
      return res.send({ noProduct: true });
    }
    return res.send(products);
  } catch (error) {
    return next(error);
  }
}

async function getProduct(req, res, next) {
  try {
    const product = await Product.findById(req.params.id);
    if (!product) {
      return res.send(`Product don't exist`);
    }
    return res.send(product);
  } catch (error) {
    return next(error);
  }
}

async function updateProduct(req, res, next) {
  if (!isAdmin(req)) {
    return res.status(401).send({ isAdmin: false });
  }
  try {
    console.log(req.body);

    let productData = {
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
      quantity: req.body.quantity,
      updated_on: new Date(),
      is_active: req.body.isActive,
    };
    if (req.body.image === "null") {
    } else {
      productData.img_path = `images/products/${req.file.filename}`;

      console.log("new product data");
      console.log(productData);
    }
    const product = await Product.findByIdAndUpdate(
      req.body.productId,
      productData
    );
    await product.save();
    return res.send(true);
  } catch (error) {
    return next(error);
  }
}

async function deleteProduct(req, res, next) {
  if (!isAdmin(req)) {
    return res
      .status(401)
      .send(
        "You are not an admin. You are not authorized to delete this product."
      );
  }
  try {
    const product = await Product.findByIdAndUpdate(req.body.productId, {
      is_active: false,
      updated_on: new Date(),
    });
    result = await product.save();
    return res.send("Product Archived");
  } catch (error) {
    return next(error);
  }
}
async function reStock(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  try {
    if (!user.isAdmin) {
      return res.send("You are not an admin, you are not allowed to re-stock");
    }
    const updateProduct = await Product.findByIdAndUpdate(req.body.productId, {
      quantity: req.body.quantity,
    });
    await updateProduct.save();
    res.send(`${updateProduct.name} new quantity is ${req.body.quantity}`);
  } catch {
    return next(error);
  }
}
module.exports = {
  createProduct: createProduct,
  getAllProducts: getAllProducts,
  getProduct: getProduct,
  updateProduct: updateProduct,
  deleteProduct: deleteProduct,
  reStock: reStock,
};
