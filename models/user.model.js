const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First name required"],
  },
  lastName: {
    type: String,
    required: [true, "Last name required"],
  },
  email: {
    type: String,
    required: [true, "Email required"],
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile No required"],
  },
  password: {
    type: String,
    required: [true, "Password required"],
  },
  address:{
    type: String,
    default: null
  },
  is_admin: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("User", userSchema);
